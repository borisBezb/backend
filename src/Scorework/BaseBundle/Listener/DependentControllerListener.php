<?php

namespace Scorework\BaseBundle\Listener;

use Scorework\BaseBundle\Controller\DependentControllerInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class DependentControllerListener
{
    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof DependentControllerInterface) {
            $controller[0]->setDependencies();
        }
    }
}