<?php
namespace Scorework\BaseBundle\Controller;

use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Scorework\CrmBundle\Service\UserService;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Translator;

abstract class ApiController extends FOSRestController implements DependentControllerInterface {
    const NOT_FOUND_MESSAGE = "controller.api.notFound";

    public function setDependencies() {

    }

    /**
     * @param $responseData
     * @param array $groups
     * @param int $statusCode
     * @param array $excludeGroups
     * @return View
     */
    public function renderResponse($responseData, $groups = [], $statusCode = Response::HTTP_OK, $excludeGroups = []) {
        $view = View::create();
        $view->setStatusCode($statusCode);
        $view->setData([
            "code" => $statusCode,
            "response" => $responseData,
        ]);


        if (count($excludeGroups) > 0) {

        }

        if(count($groups) > 0) {
            $view->getContext()->setGroups($groups);
        }

        return $view;
    }

    /**
     * @param string $message
     * @param array $translatorParams
     * @return View
     */
    public function renderNotFound($message = self::NOT_FOUND_MESSAGE, $translatorParams = []) {
        return $this->renderFail(Response::HTTP_NOT_FOUND, $message, [], $translatorParams);
    }

    /**
     * @param $message
     * @param array $translatorParams
     * @return View
     */
    public function renderBadRequest($message, $translatorParams = []) {
        return $this->renderFail(Response::HTTP_BAD_REQUEST, $message, [], $translatorParams);
    }

    /**
     * @param $message
     * @param array $params
     * @param array $translatorParams
     * @return View
     */
    public function renderInternalError($message, $params = [], $translatorParams = []) {
        return $this->renderFail(Response::HTTP_INTERNAL_SERVER_ERROR, $message, $params, $translatorParams);
    }

    /**
     * @param $message
     * @param array $translatorParams
     * @return View
     */
    public function renderTimeout($message, $translatorParams = []) {
        return $this->renderFail(Response::HTTP_REQUEST_TIMEOUT, $message, [], $translatorParams);
    }

    /**
     * @param $responseData
     * @param array $groups
     * @return View
     */
    public function renderCreated($responseData, $groups = []) {
        return $this->renderResponse($responseData, $groups, Response::HTTP_CREATED);
    }

    /**
     * @param $message
     * @param array $translatorParams
     * @return View
     */
    public function renderForbidden($message, $translatorParams = []) {
        return $this->renderFail(Response::HTTP_FORBIDDEN, $message, [], $translatorParams);
    }

    /**
     * @param $message
     * @param array $translatorParams
     * @return View
     */
    public function renderNotAuthenticated($message, $translatorParams = []) {
        return $this->renderFail(Response::HTTP_UNAUTHORIZED, $message, [], $translatorParams);
    }

    /**
     * @param $statusCode
     * @param $message
     * @param array $params
     * @param array $translatorParams
     * @return View
     */
    protected function renderFail($statusCode, $message, $params = [], $translatorParams = []) {
        $view = View::create()
            ->setStatusCode($statusCode)
            ->setData([
                    "code" => $statusCode,
                    "message" => $this->getTranslator()->trans($message, $translatorParams),
                    "params" => $params
                ]
            );

        return $view;
    }

    /**
     * @param Form $form
     * @return View
     */
    protected function renderFormError(Form $form) {
        $view =  View::create($form, Response::HTTP_BAD_REQUEST);

        return $view;
    }

    /**
     * @return View
     */
    protected function renderNoContent()
    {
        $view = View::create()->setStatusCode(Response::HTTP_NO_CONTENT);

        return $view;
    }

    /**
     * @param $serviceKey
     * @return Form
     */
    protected function getForm($serviceKey)
    {
        return $this->get($serviceKey);
    }

    /**
     * @param $serviceKey
     * @return object
     */
    protected function getCommand($serviceKey)
    {
        return $this->get($serviceKey);
    }


    /**
     * @return Translator
     */
    protected function getTranslator()
    {
        return $this->get('translator');
    }

    /**
     * @return UserService
     */
    protected function getUserService()
    {
        return $this->container->get('crm.service.user');
    }

    /**
     * @return EntityManager
     */
    protected function getEntityManager()
    {
        return $this->container->get('doctrine.orm.entity_manager');
    }
}