<?php
namespace Scorework\BaseBundle\Controller;

/**
 * Interface DependentControllerInterface
 * @package Scorework\BaseBundle\Controller
 */
interface DependentControllerInterface {
    /**
     * @void
     */
    public function setDependencies();
}