<?php
namespace Scorework\ModelBundle\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class Model
 * @Annotation
 * @Target("CLASS")
 */
class Model 
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $class;

    /**
     * @var array
     */
    protected $behaviors = [];

    /**
     * Model constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        foreach ($data as $field => $value) {
            $setter = 'set' . ucfirst($field);

            if (method_exists($this, $setter)) {
                $this->{$setter}($value);
            }
        }
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $class
     * @return $this
     */
    public function setClass($class)
    {
        $this->class = $class;

        return $this;
    }

    /**
     * @return string
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * @return array
     */
    public function getBehaviors()
    {
        return $this->behaviors;
    }
}