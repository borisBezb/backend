<?php

namespace Scorework\ModelBundle;

use Scorework\ModelBundle\DependencyInjection\BehaviorCompilerPass;
use Scorework\ModelBundle\DependencyInjection\ScenarioCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ScoreworkModelBundle extends Bundle
{
	public function build(ContainerBuilder $builder) 
	{
		parent::build($builder);
		$builder->addCompilerPass(new ScenarioCompilerPass());
		$builder->addCompilerPass(new BehaviorCompilerPass());
	}

}
