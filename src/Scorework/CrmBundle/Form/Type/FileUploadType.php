<?php
namespace Scorework\CrmBundle\Form\Type;

use Scorework\CrmBundle\Entity\File;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class FileUploadType extends AbstractType {
    /**
     * @return string
     */
    public function getBlockPrefix() {
        return '';
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('file', FileType::class, [
                'constraints' => [
                    new Assert\File([
                        "mimeTypes" => ["image/png", "image/jpeg", "image/pjpeg"],
                        "mimeTypesMessage" => "Please upload a valid image",
                        "groups" => ["image"]
                    ]),
                    new Assert\File([
                        "maxSize" => "512k",
                        "groups" => [File::TYPE_USER_LOGO]
                    ])
                ]
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => File::class,
            'csrf_protection' => false
        ));
    }
}