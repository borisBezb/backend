<?php
namespace Scorework\CrmBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRegisterType extends AbstractType {
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'userRegister';
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('email', Type\TextType::class)
            ->add('password', Type\TextType::class)
            ->add('name', Type\TextType::class)
            ->add('surname', Type\TextType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Scorework\CrmBundle\Entity\User',
        ));
    }
}