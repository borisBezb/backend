<?php
namespace Scorework\CrmBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType {
    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'company';
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('name', Type\TextType::class)
            ->add('description', Type\TextType::class)
            ->add('logo', EntityType::class, [
                'class' => 'ScoreworkCrmBundle:File',
                'choice_label' => 'id',
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Scorework\CrmBundle\Entity\Company',
        ));
    }
}