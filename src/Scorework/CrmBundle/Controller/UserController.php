<?php
namespace Scorework\CrmBundle\Controller;

use Scorework\BaseBundle\Controller\ApiController;
use Scorework\CrmBundle\Form\Type\UserRegisterType;
use Scorework\CrmBundle\{ Entity, Model };
use Scorework\ModelBundle\Component\ModelFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class UserController extends ApiController {

    /**
     * @var ModelFactoryInterface
     */
    protected $modelFactory;

    public function setDependencies() {
        $this->modelFactory = $this->get('model.factory');
    }

    /**
     * @return Model\User
     */
    protected function model() {
        return $this->modelFactory->create(Entity\User::class);
    }

    /**
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function registerAction(Request $request) {
        $model = $this->model();
        $model->setScenario('register');
        
        $model->setForm($this->createForm(UserRegisterType::class));

        if(false === $model->save()) {
            return $this->renderFormError($model->getForm());
        }

        return $this->renderResponse($model->getUser());
    }

    /**
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function loginAction(Request $request) {
        return $this->renderResponse(['token' => $this->getUserService()->getUser()->getToken()]);
    }

    /**
     * @return \FOS\RestBundle\View\View
     */
    public function verifyAction() {
        return $this->renderResponse($this->getUserService()->getUser(), ['default', 'user']);
    }
}