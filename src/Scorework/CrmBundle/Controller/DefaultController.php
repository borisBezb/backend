<?php

namespace Scorework\CrmBundle\Controller;

use Scorework\BaseBundle\Controller\ApiController;
use Scorework\CrmBundle\Entity\Worker;
use Scorework\ModelBundle\Component\Model;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends ApiController
{
    /**
     * @param $entityClass
     * @return Model
     */
    protected function model($entityClass) {
        return $this->get('model.factory')->create($entityClass);    
    }
    
    public function indexAction()
    {
        $worker = $this->model(Worker::class);
        $worker->getEntity()->setName('Boris');
        $worker->setScenario('a');
        
        $worker->update();
        var_dump(55);
        exit;
    }
}
