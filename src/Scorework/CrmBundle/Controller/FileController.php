<?php
namespace Scorework\CrmBundle\Controller;

use FOS\RestBundle\View\View;
use Scorework\BaseBundle\Controller\ApiController;
use Scorework\CrmBundle\Form\Type\FileUploadType;
use Scorework\CrmBundle\{ Entity, Model };
use Scorework\ModelBundle\Component\ModelFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class FileController extends ApiController {
    /**
     * @var ModelFactoryInterface
     */
    protected $modelFactory;

    public function setDependencies() {
        $this->modelFactory = $this->get('model.factory');
    }

    /**
     * @return Model\File
     */
    protected function model() {
        return $this->modelFactory->create(Entity\File::class);
    }
    
    /**
     * @param Request $request
     * @param $fileType
     * @param $itemType
     * @return View
     */
    public function uploadAction(Request $request, $fileType, $itemType) {

        if (false === in_array($fileType, Model\File::$acceptScenarios)) {
            return $this->renderNotFound();
        }

        if (false === in_array($itemType, Entity\File::$acceptedTypes)) {
            return $this->renderNotFound();
        }

        $model = $this->model();
        $model->setScenario($fileType);
        $model->getFile()->setType($itemType);

        $form = $this->createForm(FileUploadType::class, $model->getFile(), [
            'validation_groups' => [$fileType, $itemType]
        ]);

        $model->setForm($form);

        if(false === $model->save()) {
            return $this->renderFormError($form);
        }

        return $this->renderCreated($model->getFile());
    }
}