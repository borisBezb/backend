<?php
namespace Scorework\CrmBundle\Controller;

use Scorework\BaseBundle\Controller\ApiController;
use Scorework\CrmBundle\Entity\Company;
use Scorework\CrmBundle\Form\Type\CompanyType;
use Scorework\CrmBundle\{ Entity, Model };
use Scorework\ModelBundle\Component\ModelFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class CompanyController extends ApiController {

    /**
     * @var ModelFactoryInterface
     */
    protected $modelFactory;

    public function setDependencies() {
        $this->modelFactory = $this->get('model.factory');
    }

    /**
     * @return Model\Company
     */
    protected function model() {
        return $this->modelFactory->create(Entity\Company::class);
    }

    /**
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function createAction(Request $request) {
        $model = $this->model();

        $form = $this->createForm(CompanyType::class, $model->getCompany());
        $model->setForm($form);
        $model->setScenario('create');

        if(false === $model->save()) {
            return $this->renderFormError($form);
        }

        return $this->renderResponse($model->getCompany());
    }

    /**
     * @param Request $request
     * @return \FOS\RestBundle\View\View
     */
    public function updateAction(Request $request, $id) {
        $model = $this->model();
        $model->findById($id);
        
        if (!$model->getCompany()) {
            return $this->renderNotFound();
        }

        $form = $this->createForm(CompanyType::class, $model->getCompany(), [
            'method' => 'PUT'
        ]);
        $model->setForm($form);

        if(false === $model->save()) {
            return $this->renderFormError($form);
        }

        return $this->renderResponse($model->getCompany());
    }
    
    public function getAction(Request $request, $id)
    {
        $model = $this->model();
        if (!$model->findById($id)) {
            return $this->renderNotFound();
        }
        
        return $this->renderResponse($model->getCompany());
    }
}