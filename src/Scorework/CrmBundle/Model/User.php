<?php
namespace Scorework\CrmBundle\Model;

use Scorework\CrmBundle\Entity;
use Scorework\ModelBundle\Component\Model;

class User extends Model {
    /**
     * @return Entity\User
     */
    public function getUser() {
        return $this->entity;
    }
}