<?php
namespace Scorework\CrmBundle\Model;

use Scorework\CrmBundle\Entity;
use Scorework\ModelBundle\Component\Model;

class Lady extends Model {
    /**
     * @return Entity\Worker
     */
    public function getWorker() {
        return $this->entity;
    }
}