<?php
namespace Scorework\CrmBundle\Model;

use Scorework\CrmBundle\Entity;
use Scorework\ModelBundle\Component\Model;

/**
 * Class Company
 * @package Scorework\CrmBundle\Model
 */
class Company extends Model {
    /**
     * @return Entity\Company
     */
    public function getCompany() {
        return $this->entity;
    }
}