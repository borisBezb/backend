<?php
namespace Scorework\CrmBundle\Model;

use Scorework\CrmBundle\Entity;
use Scorework\ModelBundle\Component\Model;

class File extends Model {
    public static $acceptScenarios = ['image'];

    /**
     * @return Entity\File
     */
    public function getFile() {
        return $this->entity;
    }
}