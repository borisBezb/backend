<?php
namespace Scorework\CrmBundle\Scenario\File;

use Scorework\CrmBundle\Entity\File;
use Scorework\CrmBundle\Service\FileStorageService;
use Scorework\CrmBundle\Service\ImageService;
use Scorework\ModelBundle\Component\ModelEvent;

class Image extends Base
{
    /**
     * @var FileStorageService
     */
    protected $fileStorageService;

    /**
     * @var ImageService
     */
    protected $imageService;

    /**
     * Image constructor.
     * @param FileStorageService $fileStorageService
     * @param ImageService $imageService
     */
    public function __construct(FileStorageService $fileStorageService, ImageService $imageService)
    {
        $this->fileStorageService = $fileStorageService;
        $this->imageService = $imageService;
    }

    /**
     * @param ModelEvent $event
     * @return void
     */
    public function onBeforeSave(ModelEvent $event)
    {
        $model = $this->getModel($event);
        $file = $model->getFile();

        $this->fileStorageService->add($file);

        switch($file->getType()) {
            case File::TYPE_USER_LOGO:
                $this->imageService->createThumbnail($this->fileStorageService->getRealFilePath($file->getPath()), 80, 80);
            break;
        }
    }
}