<?php
namespace Scorework\CrmBundle\Scenario\File;

use Scorework\CrmBundle\Service\FileStorageService;
use Scorework\ModelBundle\Component\ModelEvent;

class Simple extends Base
{
    /**
     * @var FileStorageService
     */
    protected $fileStorageService;

    /**
     * Image constructor.
     * @param FileStorageService $fileStorageService
     */
    public function __construct(FileStorageService $fileStorageService)
    {
        $this->fileStorageService = $fileStorageService;
    }

    /**
     * @param ModelEvent $event
     * @return void
     */
    public function onBeforeSave(ModelEvent $event)
    {
        $model = $this->getModel($event);
        $file = $model->getFile();

        $this->fileStorageService->add($file);
    }
}