<?php
namespace Scorework\CrmBundle\Scenario\File;

use Scorework\CrmBundle\Model;
use Scorework\ModelBundle\Component\ModelEvent;
use Scorework\ModelBundle\Component\Scenario;

abstract class Base extends Scenario
{
    /**
     * @param ModelEvent $event
     * @return Model\File
     */
    protected function getModel(ModelEvent $event) {
        return $event->getModel();
    }
}