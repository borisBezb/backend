<?php
namespace Scorework\CrmBundle\Scenario\Tst;

use Scorework\CrmBundle\Entity\Lady;
use Scorework\CrmBundle\Model;
use Scorework\ModelBundle\Component\{
    ModelEvent, ModelFactoryInterface, Scenario
};

class A extends Scenario
{
    static public $name;
    /**
     * @var ModelFactoryInterface
     */
    protected $modelFactory;

    public function __construct(ModelFactoryInterface $modelFactory)
    {
        $this->modelFactory = $modelFactory;
    }

    /**
     * @param ModelEvent $event
     */
    public function onBeforeSave(ModelEvent $event)
    {
        $this->debug('before_save_a1');

        $event->getModel()->setScenario('b');
        $event->getModel()->update();
        $event->getModel()->setScenario('a');

        parent::onBeforeSave($event); // TODO: Change the autogenerated stub
    }

    public function onAfterSave(ModelEvent $event)
    {
        $this->debug('after_save_a');
        parent::onAfterSave($event); // TODO: Change the autogenerated stub
    }

    protected function debug($event) {
        var_dump('model: ' . static::$modelName . '; scenario: ' . static::$name . '; ' . $event);
    }
}