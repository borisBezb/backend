<?php
namespace Scorework\CrmBundle\Scenario\User;

use Scorework\CrmBundle\Entity;
use Scorework\CrmBundle\Service\UserService;
use Scorework\ModelBundle\Component\ModelEvent;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Register extends BaseScenario {
    /**
     * @var UserPasswordEncoderInterface
     */
    protected $encoder;

    /**
     * @var UserService
     */
    protected $userService;

    public function __construct(UserPasswordEncoderInterface $encoder, UserService $userService)
    {
        $this->encoder = $encoder;
        $this->userService = $userService;
    }

    /**
     * @param ModelEvent $event
     * @return void
     */
    public function onBeforeSave(ModelEvent $event)
    {
        $model = $this->model($event);
        $user = $model->getUser();

        $encodedPassword = $this->encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($encodedPassword);

        $userRole = $this->em->getRepository('ScoreworkCrmBundle:UserRole')->findOneBy(['code' => Entity\UserRole::ROLE_MEMBER]);
        $user->addRole($userRole);

    }

    /**
     * @param ModelEvent $event
     * @return void
     */
    public function onAfterSave(ModelEvent $event)
    {
        $model = $this->model($event);
        $user = $model->getUser();

        $this->userService->login($user);
    }
}