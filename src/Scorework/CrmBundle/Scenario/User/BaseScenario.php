<?php
namespace Scorework\CrmBundle\Scenario\User;

use Doctrine\ORM\EntityManager;
use Scorework\CrmBundle\Model;
use Scorework\ModelBundle\Component\ModelEvent;
use Scorework\ModelBundle\Component\Scenario;

abstract class BaseScenario extends Scenario {
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param EntityManager $em
     */
    public function setEntityManager(EntityManager $em) {
        $this->em = $em;
    }

    /**
     * @param ModelEvent $event
     * @return Model\User
     */
    protected function model(ModelEvent $event) {
        return $event->getModel();
    }
}