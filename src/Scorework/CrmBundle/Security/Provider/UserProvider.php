<?php
namespace Scorework\CrmBundle\Security\Provider;

use Scorework\CrmBundle\Entity;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface {
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * UserProvider constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $username
     * @return Entity\User
     */
    public function loadUserByUsername($username)
    {
        $user = $this->em->getRepository('ScoreworkCrmBundle:User')->findOneBy(['email' => $username]);

        if(!$user instanceof Entity\User) {
            throw new UsernameNotFoundException();
        }

        return $user;
    }

    /**
     * @param UserInterface $user
     * @return void
     */
    public function refreshUser(UserInterface $user)
    {
        throw new UnsupportedUserException();
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return 'Scorework\CrmBundle\Entity\User' === $class;
    }

}