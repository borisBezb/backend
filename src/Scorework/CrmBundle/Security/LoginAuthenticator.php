<?php
namespace Scorework\CrmBundle\Security;

use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Scorework\CrmBundle\Entity;
use Scorework\CrmBundle\Security\Provider\UserProvider;
use Scorework\CrmBundle\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class LoginAuthenticator extends AbstractGuardAuthenticator {
    /**
     * @var UserProvider
     */
    protected $userProvider;

    /**
     * @var UserPasswordEncoder
     */
    protected $encoder;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * LoginAuthenticator constructor.
     * @param UserProvider $userProvider
     * @param UserPasswordEncoder $encoder
     * @param UserService $userService
     */
    public function __construct(
        UserProvider $userProvider,
        UserPasswordEncoder $encoder,
        UserService $userService
    )
    {
        $this->userProvider = $userProvider;
        $this->encoder = $encoder;
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return array|void
     */
    public function getCredentials(Request $request)
    {
        if ($request->getPathInfo() != '/api/v1/user/login') {
            return;
        }

        // What you return here will be passed to getUser() as $credentials
        return [
            'email' => $request->request->get('email'),
            'password' => $request->request->get('password')
        ];
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return UserInterface
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $username = $credentials['email'];
        
        $user = $this->userProvider->loadUserByUsername($username);
        $this->userService->setUser($user);


        return $user;
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        if(false === $this->encoder->isPasswordValid($user, $credentials['password'])) {
            return false;
        }

        return true;
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $this->userService->login($token->getUser());
        return;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        );

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return JsonResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            // you might translate this message
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }
}