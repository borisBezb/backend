<?php
namespace Scorework\CrmBundle\Security;

use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Exception\JWTDecodeFailureException;
use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Scorework\CrmBundle\Security\Provider\TokenProvider;
use Scorework\CrmBundle\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuthenticator extends AbstractGuardAuthenticator {
    /**
     * @var JWTEncoderInterface
     */
    protected $encoder;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * TokenAuthenticator constructor.
     * @param JWTEncoderInterface $encoder
     * @param UserService $userService
     * @param EntityManager $em
     */
    public function __construct(JWTEncoderInterface $encoder, UserService $userService, EntityManager $em)
    {
        $this->encoder = $encoder;
        $this->userService = $userService;
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @return array|void
     */
    public function getCredentials(Request $request)
    {
        $extractor = new AuthorizationHeaderTokenExtractor('Bearer', 'Authorization');

        $token = $extractor->extract($request);

        if (false === $token) {
            // no token? Return null and no other methods will be called
            return null;
        }

        // What you return here will be passed to getUser() as $credentials
        return $token;
    }

    /**
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     * @return UserInterface
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        try {
            $tokenData = $this->encoder->decode($credentials);

            if(!$tokenData) {
                return;
            }
        } catch (JWTDecodeFailureException $e) {
            return;
        }

        // if null, authentication will fail
        // if a User object, checkCredentials() is called
        $user = $this->em->getRepository('ScoreworkCrmBundle:User')->find($tokenData['id']);
        if(!$user) {
            return;
        }

        $this->userService->setUser($user);

        return $user;
    }

    /**
     * @param mixed $credentials
     * @param UserInterface $user
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey
     * @return null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /**
     * @param Request $request
     * @param AuthenticationException $exception
     * @return JsonResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = array(
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        );

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * @param Request $request
     * @param AuthenticationException|null $authException
     * @return JsonResponse
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            // you might translate this message
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }
}