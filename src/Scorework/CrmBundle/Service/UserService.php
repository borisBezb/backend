<?php
namespace Scorework\CrmBundle\Service;

use Doctrine\ORM\EntityManager;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Scorework\CrmBundle\Entity;

class UserService
{
    /**
     * @var Entity\User
     */
    protected $user;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var JWTEncoderInterface
     */
    protected $jwtEncoder;

    /**
     * UserService constructor.
     * @param EntityManager $em
     * @param JWTEncoderInterface $jwtEncoder
     */
    public function __construct(EntityManager $em, JWTEncoderInterface $jwtEncoder)
    {
        $this->em = $em;
        $this->jwtEncoder = $jwtEncoder;
    }

    /**
     * @param Entity\User $user
     */
    public function setUser(Entity\User $user) {
        $this->user = $user;
    }

    /**
     * @return Entity\User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param Entity\User $user
     */
    public function login(Entity\User $user) {
        $jwtToken = $this->jwtEncoder->encode([
            'id' => $user->getId()
        ]);

        $user->setToken($jwtToken);
    }
}