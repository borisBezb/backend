<?php
namespace Scorework\CrmBundle\Service;

use Doctrine\ORM\EntityManager;
use Scorework\CrmBundle\Entity\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileStorageService
{
    const CHUNK_SIZE = 500;

    /**
     * @var string
     */
    protected $uploadPath;

    /**
     * @var string
     */
    protected $webPath;

    /**
     * FileStorageService constructor.
     * @param $uploadPath
     * @param $webPath
     */
    public function __construct($uploadPath, $webPath)
    {
        $this->uploadPath = $uploadPath;
        $this->webPath = $webPath;
    }

    public function add(File $file)
    {
        $fileInstance = $file->getFile();

        $clearName = substr(md5($fileInstance->getClientOriginalName() . uniqid()), 0, 10);
        $fileName =  $clearName . '.' . $fileInstance->guessExtension();
        $chunkDir = $this->buildChunkDir($fileInstance);

        $file
            ->setPath($this->buildWebPath($chunkDir, $fileName))
            ->setSize($fileInstance->getSize())
            ->setMimeType($fileInstance->getMimeType())
        ;

        $fileInstance->move(join(DIRECTORY_SEPARATOR, [$this->uploadPath, $chunkDir]), $fileName);
    }

    public function getRealFilePath($webPath) {
        $pos = strrpos($this->uploadPath, '/');
        
        return substr($this->uploadPath, 0, $pos) . $webPath;
    }

    /**
     * @param UploadedFile $file
     * @return int
     */
    protected function buildChunkDir(UploadedFile $file) {
        $checksum = crc32($file->getClientOriginalName());
        return $checksum % self::CHUNK_SIZE;
    }

    /**
     * @param $chunk
     * @param $fileName
     * @return string
     */
    protected function buildWebPath($chunk, $fileName) {
        return join(DIRECTORY_SEPARATOR, [$this->webPath, $chunk, $fileName]);
    }
}