<?php
namespace Scorework\CrmBundle\Service;

class ImageService
{
    public function createThumbnail($origin, $width, $height)
    {
        $img = new \Imagick($origin);
        $img->cropThumbnailImage($width, $height);

        $thumbnailPath = $this->getThumbnailPath($origin, $width, $height);
        $img->writeImage($thumbnailPath);
    }

    /**
     * @param $origin
     * @param $width
     * @param $height
     * @return string
     */
    public function getThumbnailPath($origin, $width, $height)
    {
        $extPosition = strrpos($origin, '.');
        $extension = substr($origin, $extPosition);
        $nameWithoutExt = substr($origin, 0, $extPosition);
        $suffix = '_' . $width . 'x' . $height;

        return $nameWithoutExt . $suffix . '.' .$extension;
    }

}