<?php

namespace Scorework\CrmBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="crm.company_user")
 */
class CompanyUser
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="users")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     * @var Company
     */
    protected $company;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="companies")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE", nullable=false)
     * @var User
     */
    protected $user;

    /**
     * @ORM\Column(type="text", nullable=false)
     * @var string
     */
    protected $role;

    /**
     * Set role
     *
     * @param string $role
     *
     * @return CompanyUser
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set company
     *
     * @param \Scorework\CrmBundle\Entity\Company $company
     *
     * @return CompanyUser
     */
    public function setCompany(\Scorework\CrmBundle\Entity\Company $company)
    {
        $this->company = $company;
        $company->addUser($this);
        
        return $this;
    }

    /**
     * Get company
     *
     * @return \Scorework\CrmBundle\Entity\Company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set user
     *
     * @param \Scorework\CrmBundle\Entity\User $user
     *
     * @return CompanyUser
     */
    public function setUser(\Scorework\CrmBundle\Entity\User $user)
    {
        $this->user = $user;
        $user->addCompany($this);

        return $this;
    }

    /**
     * Get user
     *
     * @return \Scorework\CrmBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
