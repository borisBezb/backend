<?php

namespace Scorework\CrmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Scorework\ModelBundle\Annotation\Model;

/**
 * @ORM\Entity
 * @ORM\Table(name="crm.company")
 * @Model(class="Scorework\CrmBundle\Model\Company")
 */
class Company
{
    use TimestampableEntity;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank()
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id", nullable=true)
     *
     * @var User
     */
    protected $author;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @var string
     */
    protected $description;

    /**
     * @ORM\OneToOne(targetEntity="File")
     * @ORM\JoinColumn(name="logo", referencedColumnName="id", nullable=true)
     *
     * @var File
     */
    protected $logo;

    /**
     * @ORM\OneToMany(targetEntity="CompanyUser", mappedBy="company", cascade={"persist"})
     *
     * @var ArrayCollection
     */
    protected $users;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set author
     *
     * @param \Scorework\CrmBundle\Entity\User $author
     *
     * @return Company
     */
    public function setAuthor(\Scorework\CrmBundle\Entity\User $author = null)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \Scorework\CrmBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Company
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * Add user
     *
     * @param \Scorework\CrmBundle\Entity\CompanyUser $user
     *
     * @return Company
     */
    public function addUser(\Scorework\CrmBundle\Entity\CompanyUser $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \Scorework\CrmBundle\Entity\CompanyUser $user
     */
    public function removeUser(\Scorework\CrmBundle\Entity\CompanyUser $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return CompanyUser[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set logo
     *
     * @param \Scorework\CrmBundle\Entity\File $logo
     *
     * @return Company
     */
    public function setLogo(\Scorework\CrmBundle\Entity\File $logo = null)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return \Scorework\CrmBundle\Entity\File
     */
    public function getLogo()
    {
        return $this->logo;
    }
}
