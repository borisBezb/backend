<?php

namespace Scorework\CrmBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Scorework\ModelBundle\Annotation\Model;

/**
 * @ORM\Entity
 * @ORM\Table(name="crm.users")
 * @UniqueEntity(
 *     fields="email",
 *     errorPath="email",
 *     message="Такой пользователь уже зарегистрирован в системе"
 * )
 * @Model(name="user", class="Scorework\CrmBundle\Model\User")
 */
class User implements UserInterface
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false, unique=true)
     *
     * @Assert\NotBlank()
     * @Assert\Email()
     *
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @Assert\NotBlank()
     * @Assert\Length(min = 3)
     *
     * @var string
     */
    protected $password;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\NotBlank()
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Assert\NotBlank()
     *
     * @var string
     */
    protected $surname;

    /**
     * @ORM\OneToOne(targetEntity="File")
     * @ORM\JoinColumn(name="photo", referencedColumnName="id", nullable=true)
     *
     * @var File
     */
    protected $photo;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     * @var \DateTime
     */
    protected $registeredAt;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     * @var \DateTime
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity="UserRole")
     * @ORM\JoinTable(name="crm.user_role_assoc",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     *
     * @var ArrayCollection $roles
     */
    protected $roles;

    /**
     * @ORM\OneToMany(targetEntity="CompanyUser", mappedBy="user")
     *
     * @var ArrayCollection
     */
    protected $companies;

    /**
     * @ORM\OneToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="default_company", referencedColumnName="id", nullable=true)
     *
     * @var Company
     */
    protected $defaultCompany;

    /**
     * @var string
     */
    protected $token;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
        $this->companies = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     * @return $this
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt()
    {
        return $this->registeredAt;
    }

    /**
     * @param $registeredAt
     * @return $this
     */
    public function setRegisteredAt($registeredAt)
    {
        $this->registeredAt = $registeredAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param $updatedAt
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Add role
     *
     * @param \Scorework\CrmBundle\Entity\UserRole $role
     *
     * @return User
     */
    public function addRole(\Scorework\CrmBundle\Entity\UserRole $role)
    {
        $this->roles[] = $role;

        return $this;
    }

    /**
     * Remove role
     *
     * @param \Scorework\CrmBundle\Entity\UserRole $role
     */
    public function removeRole(\Scorework\CrmBundle\Entity\UserRole $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Get roles
     *
     * @return UserRole[]
     */
    public function getRoles()
    {
        return $this->roles->toArray();
    }

    /**
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return UserToken
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }
    
    /**
     * Add company
     *
     * @param \Scorework\CrmBundle\Entity\CompanyUser $company
     *
     * @return User
     */
    public function addCompany(\Scorework\CrmBundle\Entity\CompanyUser $company)
    {
        $this->companies[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param \Scorework\CrmBundle\Entity\CompanyUser $company
     */
    public function removeCompany(\Scorework\CrmBundle\Entity\CompanyUser $company)
    {
        $this->companies->removeElement($company);
    }

    /**
     * Get companies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * Set defaultCompany
     *
     * @param \Scorework\CrmBundle\Entity\Company $defaultCompany
     *
     * @return User
     */
    public function setDefaultCompany(\Scorework\CrmBundle\Entity\Company $defaultCompany = null)
    {
        $this->defaultCompany = $defaultCompany;

        return $this;
    }

    /**
     * Get defaultCompany
     *
     * @return \Scorework\CrmBundle\Entity\Company
     */
    public function getDefaultCompany()
    {
        return $this->defaultCompany;
    }

    /**
     * Set photo
     *
     * @param \Scorework\CrmBundle\Entity\File $photo
     *
     * @return User
     */
    public function setPhoto(\Scorework\CrmBundle\Entity\File $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return \Scorework\CrmBundle\Entity\File
     */
    public function getPhoto()
    {
        return $this->photo;
    }
}
